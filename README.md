<!--required for Modrinth centering -->
<center>
<div style="text-align:center;line-height:250%">

## Discerning Furnace

[![Minecraft versions](https://cf.way2muchnoise.eu/versions/Minecraft_discerning-furnace_all.svg)](https://modrinth.com/mod/discerning-furnace/versions#all-versions)
![environment: any](https://img.shields.io/badge/environment-any-707070)  
[![loader: Fabric](https://img.shields.io/badge/loader-Fabric-cdc4ae?logo=data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMjYgMjgiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sOnNwYWNlPSJwcmVzZXJ2ZSIgc3R5bGU9ImZpbGwtcnVsZTpldmVub2RkO2NsaXAtcnVsZTpldmVub2RkO3N0cm9rZS1saW5lam9pbjpyb3VuZDtzdHJva2UtbWl0ZXJsaW1pdDoyIj48cGF0aCBkPSJtMTAgMjQtOC04TDE0IDRWMmgybDYgNnY0TDEwIDI0eiIgc3R5bGU9ImZpbGw6I2RiZDBiNCIvPjxwYXRoIGQ9Ik0xMiA2djJoLTJ2Mkg4djJINnYySDR2MkgydjRoMnYyaDJ2MmgydjJoMnYtMmgydi0yaDJ2LTJoMnYtMmgydi0yaDJ2LTRoLTJ2LTJoLTJWOGgtMlY2aDJ2MmgydjJoMnYyaDJ2Mmgydi0yaC0yVjhoLTJWNmgtMlY0aC0yVjJoMnYyaDJ2MmgydjJoMnYyaDJ2NGgtMnYyaC00djJoLTJ2MmgtMnYyaC0ydjRoLTJ2Mkg4di0ySDZ2LTJINHYtMkgydi0ySDB2LTRoMnYtMmgydi0yaDJ2LTJoMlY4aDJWNmgyVjJoMlYwaDJ2MmgtMnY0aC0yIiBzdHlsZT0iZmlsbDojMzgzNDJhIi8+PHBhdGggc3R5bGU9ImZpbGw6IzgwN2E2ZCIgZD0iTTIyIDEyaDJ2MmgtMnoiLz48cGF0aCBkPSJNMiAxOGgydjJoMnYyaDJ2MmgydjJIOHYtMkg2di0ySDR2LTJIMnYtMiIgc3R5bGU9ImZpbGw6IzlhOTI3ZSIvPjxwYXRoIGQ9Ik0yIDE2aDJ2MmgydjJoMnYyaDJ2Mkg4di0ySDZ2LTJINHYtMkgydi0yeiIgc3R5bGU9ImZpbGw6I2FlYTY5NCIvPjxwYXRoIGQ9Ik0yMiAxMnYtMmgtMlY4aC0yVjZsLTIuMDIzLjAyM0wxNiA4aDJ2MmgydjJoMnpNMTAgMjR2LTJoMnYtNGg0di00aDJ2LTJoMnY0aC0ydjJoLTJ2MmgtMnYyaC0ydjJoLTIiIHN0eWxlPSJmaWxsOiNiY2IyOWMiLz48cGF0aCBkPSJNMTQgMThoLTR2LTJIOHYtMmgydjJoNHYyem00LTRoLTR2LTJoLTJ2LTJoLTJWOGgydjJoMnYyaDR2MnpNMTQgNGgydjJoLTJWNHoiIHN0eWxlPSJmaWxsOiNjNmJjYTUiLz48L3N2Zz4=)](https://fabricmc.net/)
<a href="https://quiltmc.org/"><img alt="available for: Quilt Loader" width=73 src="https://raw.githubusercontent.com/QuiltMC/art/master/brand/1024png/quilt_available_dark.png"></a>  
[![license: MIT](https://img.shields.io/badge/license-MIT-white)](https://gitlab.com/supersaiyansubtlety/discerning_furnace/-/blob/master/LICENSE)
[![source on: GitLab](https://img.shields.io/badge/source_on-GitLab-fc6e26?logo=gitlab)](https://gitlab.com/supersaiyansubtlety/discerning_furnace)
[![issues: GitLab](https://img.shields.io/gitlab/issues/open-raw/supersaiyansubtlety/discerning_furnace?label=issues&logo=gitlab)](https://gitlab.com/supersaiyansubtlety/discerning_furnace/-/issues)
[![localized: Percentage](https://badges.crowdin.net/discerning-furnace/localized.svg)](https://crwd.in/discerning-furnace)  
[![Modrinth: Downloads](https://img.shields.io/modrinth/dt/discerning-furnace?logo=modrinth&label=Modrinth&color=00ae5d)](https://modrinth.com/mod/discerning-furnace/versions)
[![CurseForge: Downloads](https://img.shields.io/curseforge/dt/428966?logo=curseforge&label=CurseForge&color=f16437)](https://www.curseforge.com/minecraft/mc-mods/discerning-furnace/files)  
[![chat: Discord](https://img.shields.io/discord/1006391289006280746?logo=discord&color=5964f3)](https://discord.gg/xABmPngXAH)
<a href="https://coindrop.to/supersaiyansubtlety"><img alt="coindrop.to me" width="82" style="border-radius:3px" src="https://coindrop.to/embed-button.png"></a>
<a href="https://ko-fi.com/supersaiyansubtlety"><img alt="Buy me a coffee" width="106" src="https://i.ibb.co/4gwRR8L/p.png"></a>

</div>
</center>

---

### Only allow smeltable items in furnaces.

Works client-side, server-side, and in single player.  
On clients, it prevents the player from inserting unsmeltable items though GUIs.  
On servers, it prevents hoppers and other blocks from inserting unsmeltable items.

Prior to version 1.0.8 there was no client-side functionality, only server-sided block insertions where handled.

[zephaniahnoah](https://www.curseforge.com/members/zephaniahnoah/projects) has also created a
[Forge port](https://www.curseforge.com/minecraft/mc-mods/discerning-furnace-forge).

This tiny mod (~32 lines of code) prevents un-smeltable items from inserting to the input slot of a furnace.

This is useful for sorting items and for preventing clogs in furnace arrays.

It affects all vanilla furnaces, and modded furnaces that don't have their own way of limiting inputs should work as
well.

---

<details>

<summary>Translating</summary>

[![localized: Percentage](https://badges.crowdin.net/discerning-furnace/localized.svg)](https://crwd.in/discerning-furnace)  
If you'd like to help translate Discerning Furnace, you can do so on
[Crowdin](https://crwd.in/discerning-furnace).

</details>

---

This mod is only for Fabric (works on Quilt, too!) and I won't be porting it to Forge. The license is MIT, however,
~~so anyone else is free to port it.~~ and [zephaniahnoah](https://www.curseforge.com/members/zephaniahnoah/projects)
has [ported it to Forge](https://www.curseforge.com/minecraft/mc-mods/discerning-furnace-forge)!

I'd appreciate links back to this page if you port or otherwise modify this project, but links aren't required.
