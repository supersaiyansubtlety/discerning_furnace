package net.sssubtlety.discerning_furnace.mixin;

import net.minecraft.block.entity.AbstractFurnaceBlockEntity;
import net.minecraft.block.entity.LockableContainerBlockEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.recipe.AbstractCookingRecipe;
import net.minecraft.recipe.RecipeManager;
import net.minecraft.recipe.SingleRecipeInput;
import net.minecraft.server.world.ServerWorld;

import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(AbstractFurnaceBlockEntity.class)
abstract class AbstractFurnaceBlockEntityMixin extends LockableContainerBlockEntity {
    @Shadow @Final private RecipeManager.CachedCheck<SingleRecipeInput, ? extends AbstractCookingRecipe> recipeCache;

    private AbstractFurnaceBlockEntityMixin() {
        //noinspection DataFlowIssue
        super(null, null, null);
        throw new IllegalStateException("AbstractFurnaceBlockEntityMixin's dummy constructor called!");
    }

    @Inject(method = "isValid", at = @At("RETURN"), cancellable = true)
    private void disallowItemsWithoutRecipes(int slot, ItemStack stack, CallbackInfoReturnable<Boolean> cir) {
        if (
            // input slot
            slot == 0 &&
            // would be valid
            cir.getReturnValue() &&
            this.world instanceof ServerWorld serverWorld &&
            // not smeltable
            this.recipeCache.getFirstMatch( new SingleRecipeInput(stack), serverWorld).isEmpty()
        ) {
            cir.setReturnValue(false);
        }
    }
}
