package net.sssubtlety.discerning_furnace.mixin.accessor;

import net.minecraft.item.ItemStack;
import net.minecraft.screen.AbstractFurnaceScreenHandler;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.gen.Invoker;

@Mixin(AbstractFurnaceScreenHandler.class)
public interface AbstractFurnaceScreenHandlerAccessor {
    @Invoker("isSmeltable")
    boolean discerning_furnace$callIsSmeltable(ItemStack itemStack);
}
