package net.sssubtlety.discerning_furnace.mixin;

import net.sssubtlety.discerning_furnace.mixin.accessor.AbstractFurnaceScreenHandlerAccessor;

import net.minecraft.client.network.ClientPlayerInteractionManager;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.screen.AbstractFurnaceScreenHandler;
import net.minecraft.screen.slot.SlotActionType;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ClientPlayerInteractionManager.class)
class ClientPlayerInteractionManagerMixin {
    @Inject(
        method = "clickSlot", cancellable = true,
        at = @At(
            value = "FIELD",
            // first thing in `else` after `syncId` check
            target = "Lnet/minecraft/screen/ScreenHandler;slots:Lnet/minecraft/util/collection/DefaultedList;"
        )
    )
    private void cancelIfUnsmeltableGoingToFurnace(
        int syncId, int slotId, int button, SlotActionType actionType, PlayerEntity player, CallbackInfo ci
    ) {
        if (!(player.currentScreenHandler instanceof AbstractFurnaceScreenHandler furnaceScreenHandler)) {
            return;
        }

        switch (actionType) {
            // QUICK_MOVE doesn't allow unsmeltable items in vanilla
            case CLONE, THROW, PICKUP_ALL, QUICK_MOVE -> { return; }
            case SWAP, PICKUP, QUICK_CRAFT -> {
                // if not input slot do nothing
                if (slotId != 0) {
                    return;
                }

                final ItemStack cursorStack = actionType == SlotActionType.SWAP ?
                    player.getInventory().getStack(button) :
                    // PICKUP or QUICK_CRAFT
                    furnaceScreenHandler.getCursorStack();

                if (
                    cursorStack.isEmpty() ||
                    ((AbstractFurnaceScreenHandlerAccessor) furnaceScreenHandler)
                        .discerning_furnace$callIsSmeltable(cursorStack)
                ) {
                    return;
                }
            }
        }

        ci.cancel();
    }
}
